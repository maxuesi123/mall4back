import { defineConfig } from 'vite';
import vue from '@vitejs/plugin-vue';
import path from 'path';
// const path = require('path');
// https://vitejs.dev/config/
export default defineConfig({
  plugins: [
    vue(),
    // eslintPlugin({
    //   cache: false,
    // }),
  ],
  resolve: {
    alias: {
      '@': path.resolve(__dirname, './src'), // 设置别名
    },
    // 省略文件后缀
    extensions: ['', '.ts', '.js', '.json', '.vue', '.scss', '.css'],
  },
  server: {
    open: true, // 启动项目自动弹出浏览器
    port: 3000, // 启动端口
    proxy: {
      '/api': {
        target: 'https://bjwz.bwie.com/mall4w', // 实际请求地址
        changeOrigin: true,
        rewrite: (path) => path.replace(/^\/api/, ''),
      },
    },
  },
});
function eslintPlugin(arg0: { cache: boolean }): import('vite').PluginOption {
  throw new Error('Function not implemented.');
}
