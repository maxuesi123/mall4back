module.exports = {
  // 设置我们的运行环境为浏览器 + es2021 + node ,否则eslint在遇到 Promise，window等全局对象时会报错
  env: {
    browser: true,
    es2021: true,
    node: true,
    // 开启setup语法糖环境
    'vue/setup-compiler-macros': true,
  },
  // 继承eslint推荐的规则集，vue基本的规则集，typescript的规则集
  extends: [
    'standard',
    'eslint:recommended',
    'plugin:@typescript-eslint/recommended',
    'plugin:vue/vue3-recommended',
    'plugin:prettier/recommended', // 新增，必须放在最后面
  ],
  // 新增，解析vue文件
  parser: 'vue-eslint-parser',
  // 支持ts的最新语法
  parserOptions: {
    ecmaVersion: 'latest',
    parser: '@typescript-eslint/parser',
    sourceType: 'module',
  },
  // 添加vue和@typescript-eslint插件，增强eslint的能力
  plugins: ['vue', '@typescript-eslint'],

  rules: {
    semi: 'off',
    'vue/no-unused-components': 'off', // 当存在定义而未使用的组件时，关闭报错
    'generator-star-spacing': 'off',
    'no-debugger': 'off',
    'no-tabs': 'off',
    'no-unused-vars': 'off',
    // 不校验ts没有使用的变量
    '@typescript-eslint/no-unused-vars': ['off'],
    // 不校验ts没用的函数（空函数）
    '@typescript-eslint/no-empty-function': ['off'],
    // 不校验console
    'no-console': 'off',
    'no-irregular-whitespace': 'off',
    // 不校验没用的函数（空函数）
    'no-empty-function': 'off',
    // 不校验组件名称
    'vue/no-reserved-component-names': 'off',
    'vue/multi-word-component-names': 'off',
    // 不校验没用的表达式
    'no-unused-expressions': 'off',
    // 不校验没用的标签
    'no-unused-labels': 'off',
    'comma-dangle': 'off',
    // 不校验相同的item
    'vue/no-template-shadow': 'off',
    'vue/no-mutating-props': 'off',
    'vue/require-prop-types': 'off',
    'vue/no-setup-props-destructure': 'off',
    // 关闭any类型的警告：
    '@typescript-eslint/no-explicit-any': ['off'],
    'no-use-before-define': 'off',
    '@typescript-eslint/no-use-before-define': ['error'],
    'vue/v-on-event-hyphenation': 'off',
    'vue/no-lone-template': 'off',
  },
};
