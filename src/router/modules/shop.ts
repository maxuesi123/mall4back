const list = [
  // 公告
  {
    path: '/shop-notice',
    name: '公告管理',
    component: () => import('@/view/Index/shop_file/Affiche/Affiche.vue'),
  },
  //  热搜
  {
    path: '/shop-hotSearch',
    name: '热搜管理',
    component: () => import('@/view/Index/shop_file/Hotbar/Hotbar.vue'),
  },
  //  轮播图
  {
    path: '/admin-indexImg',
    name: '轮播图管理',
    component: () => import('@/view/Index/shop_file/Swipers/Swipers.vue'),
  },
  // 运费
  {
    path: '/shop-transport',
    name: '运费管理',
    component: () => import('@/view/Index/shop_file/Freights/Freights.vue'),
  },
  // 自提点 Selfpick-up
  {
    path: '/shop-pickAddr',
    name: '自提点管理',
    component: () =>
      import('@/view/Index/shop_file/Selfpick-up/Selfpick-up.vue'),
  },
];
export default list;
