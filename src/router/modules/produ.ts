const list = [
  // 订单 Orderform
  // 分组管理
  {
    path: '/prod-prodTag',
    name: '分组管理',
    component: () => import('@/view/Index/product_file/Grouping/Grouping.vue'),
  },
  // 产品管理
  {
    path: '/prod-prodList',
    name: '产品管理',
    component: () => import('@/view/Index/product_file/Product/Product.vue'),
  },
  // 分类管理
  {
    path: '/prod-category',
    name: '分类管理',
    component: () => import('@/view/Index/product_file/Classify/Classify.vue'),
  },
  // 评论管理
  {
    path: '/prod-prodComm',
    name: '评论管理',
    component: () => import('@/view/Index/product_file/Comment/Comment.vue'),
  },
  // 规格管理
  {
    path: '/prod-spec',
    name: '规格管理',
    component: () =>
      import('@/view/Index/product_file/Specification/Specification.vue'),
  },
];
export default list;
