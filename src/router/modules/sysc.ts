const list = [
  // 地址 Address
  {
    path: '/sys-area',
    name: '地址管理',
    component: () => import('@/view/Index/system_file/Address/Address.vue'),
  },
  // 管理员 Administrator
  {
    path: '/sys-user',
    name: '管理员列表',
    component: () =>
      import('@/view/Index/system_file/Administrator/Administrator.vue'),
  },
  // 角色 Role
  {
    path: '/sys-role',
    name: '角色管理',
    component: () => import('@/view/Index/system_file/Role/Role.vue'),
  },
  // 菜单
  {
    path: '/sys-menu',
    name: '菜单管理',
    component: () => import('@/view/Index/system_file/Menu/Menu.vue'),
  },
  // 定时任务  Timed
  {
    path: '/sys-schedule',
    name: '定时任务',
    component: () => import('@/view/Index/system_file/Timed/Timed.vue'),
  },
  // 参数 Parameters
  {
    path: '/sys-config',
    name: '参数管理',
    component: () =>
      import('@/view/Index/system_file/Parameters/Parameters.vue'),
  },
  // 系统  System_log
  {
    path: '/sys-log',
    name: '系统日志',
    component: () =>
      import('@/view/Index/system_file/System_log/System_log.vue'),
  },
];
export default list;
