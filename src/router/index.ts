import { Shop } from '@element-plus/icons-vue';
import { createRouter, createWebHistory, RouteRecordRaw } from 'vue-router';
// 目前是把所有路由都放在路由表里面了,并没有烤炉是否是不同会员还是管理员
// 问题 加入登录的这个人是普通会员的话，当他手动修改url路由的时候，就会显示管理员的页面
const routes: RouteRecordRaw[] = [
  // 重定向
  {
    path: '/',
    redirect: '/home',
  },
  {
    path: '/index',
    name: 'index',
    component: () => import('@/view/Index.vue'),
    children: [
      // 首页
      {
        path: '/home',
        name: '首页',
        component: () => import('@/view/Index/Home/Home.vue'),
      },
    ],
  },
  // 登录
  {
    path: '/login',
    name: '登录',
    component: () => import('@/view/Login/Login.vue'),
  },
  // 404页面找不到   匹配所有路径 vue2使用*  vue3使用/：pathMatch(.*)*  或/:pathMatch(.*) 或 /.catchAll(.*)
  {
    path: '/:pathMatch(.*)',
    component: () => import('@/view/NoFound/NoFoun.vue'),
  },
];
const router = createRouter({
  routes,
  history: createWebHistory(),
});
export default router;
