import httpRequest from '@/utils/index';
export const getList = (params: any) =>
  httpRequest.get({
    url: '/sys/user/page',
    params,
  });
