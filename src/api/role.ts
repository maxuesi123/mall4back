import httpRequest from '@/utils/index';
export const getroleList = (params: any) => {
  // console.log(params, 'params');

  return httpRequest.get({
    url: '/sys/role/page',
    params,
  });
};
