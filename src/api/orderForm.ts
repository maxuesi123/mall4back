import httpRequest from '@/utils/index';
// 获取数据
export const getList = (params: any) =>
  httpRequest.get({
    url: '/order/order/page',
    params,
  });

// 请求修改信息
export const getEditList = (orderNumber: number) =>
  httpRequest.get({
    url: `/order/order/orderInfo/${orderNumber}`,
  });

// 查询订单{inpval,current,size,status}:{inpval:string,current:number | undefined,size:number | undefined,status:number}
export const SearchList = (params: {
  inpval: string;
  current: number | undefined;
  size: number | undefined;
  status: number;
}) =>
  httpRequest.get({
    url: `/order/order/page`,
    params,
  });
