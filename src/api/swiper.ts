import httpRequest from '@/utils/index';
// 轮播图管理
export const getswiper = (params: any) =>
  httpRequest.get({
    // url: '/admin/indexImg/page?t=1656936928455&current=1&size=10',
    url: '/admin/indexImg/page?t=1656936928455&current=1&size=10',
    params,
  });
// 运费管理
// /shop/transport/page?t=1656943010204&current=1&size=10
export const getfreight = (params: any) =>
  httpRequest.get({
    url: '/shop/transport/page',
    params,
  });
// 自提点管理
export const getself = (params: any) =>
  httpRequest.get({
    url: '/shop/pickAddr/page',
    params,
  });
// 会员管理
export const getvip = (params: any) =>
  httpRequest.get({
    url: '/admin/user/page',
    params,
  });
