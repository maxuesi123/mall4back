import httpRequest from '@/utils/index';

// 获取表格数据
export const getProdprod = (params: any) =>
  httpRequest.get({
    url: '/prod/prod/page',
    params,
  });
