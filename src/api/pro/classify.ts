import httpRequest from '@/utils/index';

// 获取表格数据
export const getProdCategory = (params: any) =>
  httpRequest.get({
    url: '/prod/category/table',
    params,
  });
