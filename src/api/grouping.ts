import httpRequest from '@/utils/index';

// 获取表格数据
export const getProdTag = (params: any) =>
  httpRequest.get({
    url: '/prod/prodTag/page',
    params,
  });
// 新增接口
export const getAddProdTag = (params: any) =>
  httpRequest.post({
    url: '/prod/prodTag',
    data: params,
  });
// 点击修改回显数据接口
export const getProdTagItem = (params: any) =>
  httpRequest.get({
    url: `/prod/prodTag/info/${params.id}?t=${params.t}`,
    params: {},
  });
// 修改接口
export const getEditProdTag = (params: any) =>
  httpRequest.put({
    url: '/prod/prodTag',
    data: params,
  });
// 删除接口
export const getDeleteProdTag = (params: any) =>
  httpRequest.delete({
    url: `/prod/prodTag/${params.id}`,
    params,
  });
