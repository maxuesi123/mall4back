import httpRequest from '@/utils/index';
// 菜单
export const getmenu = (params: any) =>
  httpRequest.get({
    url: 'sys/menu/nav/',
    data: params,
  });
