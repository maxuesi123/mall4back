import httpRequest from '@/utils/index';
// 参数
export const getsys = (params: any) =>
  httpRequest.get({
    url: 'sys/log/page',
    data: params,
  });
