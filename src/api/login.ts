import httpRequest from '@/utils/index';
export interface ResposeData<T> {
  status: number;
  statusText: string;
  data: T;
}
export interface UserInfo {
  createTime: string;
  email: string;
  mobile: string;
  roleIdList: number[];
  shopId: number;
  status: number;
  userId: number;
  username: string;
}
export interface MenuListItem {
  icon: string;
  list: MenuListItem[];
  menuId: number;
  name: string;
  orderNum: number;
  parentId: number;
  parentName: number | null;
  perms: string;
  type: number;
  url: string;
}

export interface NavList {
  authorities: any[];
  menuList: MenuListItem[];
}
export const loginhand = (params: any) =>
  httpRequest.post({
    url: '/login?grant_type=admin',
    data: params,
  });
export const getUserInfo = (data?: any) =>
  httpRequest.get<ResposeData<UserInfo>>({
    url: '/sys/user/info',
    data,
  });

export const getMenuList = (data?: any) =>
  httpRequest.get<ResposeData<NavList>>({
    url: '/sys/menu/nav',
    data,
  });
