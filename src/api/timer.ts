import httpRequest from '@/utils/index';
// 定时任务
export const gettimed = (params: any) =>
  httpRequest.get({
    url: '/sys/schedule/page/',
    data: params,
  });
