import axios, { AxiosRequestConfig, AxiosInstance } from 'axios';
import { ElMessage, ElLoading } from 'element-plus';
import { LoadingInstance } from 'element-plus/lib/components/loading/src/loading';
import cache from '@/utils/localche/cache';
interface HttpRequestConfig extends AxiosRequestConfig {
  isShowLoading?: boolean;
}
const DEFAULT_LODAING = true;
class httpRequest {
  instance: AxiosInstance;
  isShowLoading: boolean;
  isLoading?: LoadingInstance;
  constructor(config: HttpRequestConfig) {
    this.instance = axios.create(config); // 创建了一个 axios 的实例
    this.isShowLoading = config.isShowLoading || DEFAULT_LODAING;
    // 添加请求拦截器
    this.instance.interceptors.request.use(
      (configInfo) => {
        if (this.isShowLoading) {
          this.isLoading = ElLoading.service({
            lock: true,
            text: 'Loading . . .',
            background: 'rgba(0, 0, 0, 0.7)',
          });
        }
        this.isShowLoading = false;
        // 在发送请求之前做些什么
        return {
          ...configInfo,
          headers: {
            ...configInfo.headers,
            Authorization: cache.getCache('token'),
          },
        };
      },
      (error) => {
        // 对请求错误做些什么
        return Promise.reject(error);
      }
    );

    // 添加响应拦截器   服务器响应之后，把loading关闭
    this.instance.interceptors.response.use(
      (response) => {
        this.isLoading?.close();
        // 对响应数据做点什么
        return response;
      },
      (error) => {
        console.log(error, 'error');
        this.isLoading?.close();
        if (error.response.status === 400) {
          ElMessage.error(error.response.data || '请求失败');
        } else if (error.response.status === 401) {
          ElMessage.error(error.response.data || '当前页面您无权访问');
        }
        // 对响应错误做点什么
        return Promise.reject(error);
      }
    );
  }

  request(config: HttpRequestConfig) {
    return new Promise((resolve, reject) => {
      this.instance
        .request(config)
        .then((res) => {
          // 防止下一次请求受影响
          this.isShowLoading = DEFAULT_LODAING;
          resolve(res);
        })
        .catch((err) => {
          this.isShowLoading = DEFAULT_LODAING;

          reject(err);
          return err;
        });
    });
  }

  get(config: HttpRequestConfig) {
    return this.request({ ...config, method: 'GET' });
  }

  post(config: HttpRequestConfig) {
    return this.request({ ...config, method: 'POST' });
  }

  put(config: HttpRequestConfig) {
    return this.request({ ...config, method: 'PUT' });
  }

  delete(config: HttpRequestConfig) {
    return this.request({ ...config, method: 'DELETE' });
  }
}
export default httpRequest;
