class LocalCache {
  setCache(key: string, val: any) {
    if (typeof val === 'object') {
      val = JSON.stringify(val);
    }
    window.localStorage.setItem(key, val);
  }

  getCache(key: string) {
    const val = window.localStorage.getItem(key);
    if (!val) return null;
    try {
      return JSON.parse(val);
    } catch (error) {
      return val;
    }
  }

  removeCache(key: string) {
    window.localStorage.removeItem(key);
  }

  clearCache() {
    window.localStorage.clear();
  }
}
export default new LocalCache();
