import { MenuListItem } from '@/api/login';
import type { RouteRecordRaw } from 'vue-router';
// 定义一个方法  作用 映射路由的
export function mapMenuToRouter(menuList: MenuListItem[]): RouteRecordRaw[] {
  // 最终的路由
  const routes: RouteRecordRaw[] = [];
  let allRoutes: RouteRecordRaw[] = [];
  // 批量导入  webpack
  // const routeFiles=require.context('@/router/modules/*.ts',false,/\.ts/)

  // vite import.meata.globEager
  const routeFiles = import.meta.globEager('@/router/modules/*.ts');

  allRoutes = Object.keys(routeFiles).reduce((prev: any, path: string) => {
    prev.push(...routeFiles[path].default);
    return prev;
  }, []);

  const getRoutes = (menus: MenuListItem[]) => {
    // 循环
    for (const menu of menus) {
      if (menu.type === 1) {
        const route = allRoutes.find(
          (item) => item.path === '/' + menu.url.replace('/', '-')
        );
        if (route) routes.push(route);
      } else {
        getRoutes(menu.list);
      }
    }
  };
  getRoutes(menuList);

  return routes;
}
