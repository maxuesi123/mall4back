import { createApp } from 'vue';
import App from './App.vue';
import router from './router/index';
import ElementPlus from 'element-plus';
import 'element-plus/dist/index.css';
import * as ElementPlusIconsVue from '@element-plus/icons-vue';
import UUID from 'vue-uuid';
import { store, key } from './store';
import { LoginStoreMenu } from '@/store';
const app = createApp(App);
app.use(UUID);
app.use(store, key);
LoginStoreMenu(); // 解决刷新之后,动态路由不存在的问题
app.use(router);
app.use(ElementPlus);
app.mount('#app');

for (const [key, component] of Object.entries(ElementPlusIconsVue)) {
  app.component(key, component);
}
