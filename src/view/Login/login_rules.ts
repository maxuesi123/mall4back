export const loginrules = {
  principal: [{ message: '请输入用户名', required: true, trigger: 'blur' }],
  password: [
    {
      max: 18,
      min: 6,
      required: true,
      message: '请输入密码',
      trigger: 'blur',
    },
  ],
  verification: [
    {
      max: 4,
      min: 4,
      trigger: 'blur',
      required: true,
      message: '请输入验证码',
    },
  ],
};
