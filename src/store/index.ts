import { InjectionKey } from 'vue';
import { createStore, Store, useStore as baseUseStore } from 'vuex';

import { IStateTypre } from './type';
import Loginstore from './modules/login';
// import { InjectionKey } from 'vue';

// import { State } from './type';
import administra from './modules/administra';
import Grouping from './modules/grouping';
import Tags from './modules/tabs';
// // 定义 injection key
// export const key: InjectionKey<Store<State>> = Symbol('');
// 创建一个新的 store 实例
// const store = createStore({
// import { InjectionKey } from 'vue';
// import Grouping from './modules/grouping';
// // 为 store state 声明类型
// export interface State {
//   count: number;
// }

// 定义 injection key
export const key: InjectionKey<Store<IStateTypre>> = Symbol('');

// 创建一个新的 store 实例
export const store = createStore({
  state() {
    return {
      count: 0,
    };
  },
  mutations: {
    increment(state) {
      state.count++;
    },
  },
  modules: {
    Loginstore,
    administra,
    Grouping,
    Tags,
  },
});
export function LoginStoreMenu() {
  store.dispatch('Loginstore/getLoginMenuLists');
}
// 定义自己的 `useStore` 组合式函数

//  default store;
export function useStore() {
  return baseUseStore(key);
}
