import { Module } from 'vuex';

interface IState {
  dialogVisible: boolean;
}

const Grouping: Module<IState, any> = {
  namespace: true,
  state: () => ({
    dialogVisible: false,
  }),
  getters: {},
  mutations: {},
  actions: {},
};

export default Grouping;
