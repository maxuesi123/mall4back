import { Module } from 'vuex';
import { getList } from '@/api/administrator';
import { getroleList } from '@/api/role';
interface listType {
  createTime: string;
  email: string;
  mobile: string;
  username: string;
}
const administra = {
  state: {
    data: [],
    total: 0,
  },
  mutations: {
    setState(state: any, payload: { value: any; key: any }) {
      state[payload.key] = payload.value;
    },
  },
  actions: {
    // 获取角色管理数据
    async getvalue(
      { commit }: any,
      payload: { current: number; size: number }
    ) {
      const list: any = await getList({
        current: payload.current,
        size: payload.size,
      });
      commit('setState', { key: 'data', value: list.data.records });
      commit('setState', { key: 'total', value: list.data.total });
    },
    // 获取角色管理数据
    async getList({ commit }: any, payload: { current: number; size: number }) {
      const list: any = await getroleList({
        current: payload.current,
        size: payload.size,
      });
      commit('setState', { key: 'data', value: list.data.records });
      commit('setState', { key: 'total', value: list.data.total });
    },
    // 查找角色管理数据
    async getSearch(
      { commit }: any,
      payload: { current: number; size: number; roleName: string }
    ) {
      const list: any = await getroleList({
        current: payload.current,
        size: payload.size,
        roleName: payload.roleName,
      });
      // console.log(list,'searchstore');
      commit('setState', { key: 'data', value: list.data.records });
      commit('setState', { key: 'total', value: list.data.total });
    },
    // 查找管理员列表数据
    async getUserSearch(
      { commit }: any,
      payload: { current: number; size: number; username: string }
    ) {
      const list: any = await getList({
        current: payload.current,
        size: payload.size,
        username: payload.username,
      });
      // console.log(list,'searchstore');
      commit('setState', { key: 'data', value: list.data.records });
      commit('setState', { key: 'total', value: list.data.total });
    },
  },
  namespaced: true,
};
export default administra;
