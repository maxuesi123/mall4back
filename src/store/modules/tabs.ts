/* eslint-disable @typescript-eslint/no-use-before-define */
import { Module } from 'vuex';

import cache from '@/utils/localche/cache';

const Loginstore: Module<ILoginstroe, any> = {
  namespaced: true, // 命名空间
  state: () => ({
    tagArr: cache.getCache('tagArr') || [],
    tagId: cache.getCache('tagId'),
  }),
  getters: {},
  mutations: {
    setTagArr(state, payload) {
      const list = state.tagArr.findIndex((value, index) => {
        return value.name === payload.name;
      });
      if (list === -1) {
        state.tagArr.push(payload);
      } else {
        cache.setCache('tagId', state.tagArr[state.tagArr.length - 1].length);
        state.tagArr.splice(list, 0);
      }
      cache.setCache('tagArr', state.tagArr);
    },
    removeTag(state, payload) {
      state.tagArr.forEach((value, index) => {
        if (value.name === payload) {
          state.tagArr.splice(index, 1);
        }
      });
      // eslint-disable-next-line no-self-assign
      state.tagArr = state.tagArr;
      cache.setCache('tagArr', state.tagArr);
    },
    clearTag(state, payload) {
      state.tagArr.length = 0;
      cache.setCache('tagArr', state.yagArr);
    },
  },
  actions: {},
};
export default Loginstore;
