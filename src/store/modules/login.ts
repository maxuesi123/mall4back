/* eslint-disable @typescript-eslint/no-use-before-define */
import { Module } from 'vuex';
import { loginhand, getUserInfo, getMenuList } from '@/api/login';
import cache from '@/utils/localche/cache';
import { ILoginstroe } from './type';
import { mapMenuToRouter } from '@/utils/addMenu';
import router from '@/router';
const Loginstore: Module<ILoginstroe, any> = {
  namespaced: true, // 命名空间
  state: () => ({
    token: '',
    userInfo: cache.getCache('userOnfo') || {},
    menuList: cache.getCache('menuList') || {},
  }),
  getters: {},
  mutations: {
    changToken(state, token) {
      state.token = token;
    },
    changUserInfo(state, info) {
      state.useInfo = info;
    },
    changMenuList(state, list) {
      state.menuList = list;
      // 动态添加路由
      const routes = mapMenuToRouter(list);
      console.log(routes, '*****');

      // 添加到路由表里面去
      routes.forEach((item) => {
        router.addRoute('index', item);
      });
    },
  },
  actions: {
    async loginAction({ commit }, payload) {
      // 获取token的接口
      const res = await loginhand(payload);
      commit('changToken', res.data.accsess_token);

      // 更新本地 存储本地
      cache.setCache('token', `bearer${res.data.access_token}`);

      // 获取用户信息
      const userInfos = await getUserInfo();
      commit('changUserInfo', userInfos.data);
      cache.setCache('userInfo', userInfos.data);

      // 获取菜单列表
      const meunres = await getMenuList();
      commit('changMenuList', meunres.data.menuList);
      cache.setCache('menuList', meunres.data.menuList);

      // 跳到首页
    },
    getLoginMenuLists({ commit }) {
      // 刷新的时候在请求一次
      const menuList = cache.getCache('menuList');
      if (menuList) {
        commit('changMenuList', menuList);
      }
    },
  },
};
export default Loginstore;
