// 为 store state 声明类型
import { ILoginstroe } from './modules/type';
export interface State {
  [x: string]: any;
  count: number;
}

export interface ILoginTpe {
  Loginstore: ILoginstroe;
}

export type IStateTypre = State & ILoginTpe;
